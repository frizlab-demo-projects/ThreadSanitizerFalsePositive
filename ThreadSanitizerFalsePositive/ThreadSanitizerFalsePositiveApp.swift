/*
 * ThreadSanitizerFalsePositiveApp.swift
 * ThreadSanitizerFalsePositive
 *
 * Created by François Lamboley on 2021/10/16.
 */

import SwiftUI



@main
struct ThreadSanitizerFalsePositiveApp : App {
	
	var body: some Scene {
		let _ = Task{
			await withTaskGroup(of: Void.self, body: { tg in
				tg.addTask{ await Task.sleep(UInt64(1e9)) }
				tg.addTask{ await Task.sleep(UInt64(1e9)) }
				tg.addTask{ await Task.sleep(UInt64(1e9)) }
			})
			print("all done")
		}
		
		WindowGroup {
			ContentView()
		}
	}
	
}
