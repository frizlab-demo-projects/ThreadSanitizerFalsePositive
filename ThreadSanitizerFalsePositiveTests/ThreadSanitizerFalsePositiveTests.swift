/*
 * ThreadSanitizerFalsePositiveTests.swift
 * ThreadSanitizerFalsePositiveTests
 *
 * Created by François Lamboley on 2021/10/16.
 */

import XCTest



class ThreadSanitizerFalsePositiveTests: XCTestCase {
	
	func testThreadSanitizer() async throws {
		await withTaskGroup(of: Void.self, body: { tg in
			tg.addTask{ await Task.sleep(UInt64(1e9)) }
			tg.addTask{ await Task.sleep(UInt64(1e9)) }
			tg.addTask{ await Task.sleep(UInt64(1e9)) }
		})
		print("all done")
	}
	
}
